package com.example.eemeli.gonarss;

import android.util.Log;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;


public class FeedReader implements Runnable {
    private URL url;

    public FeedReader(String u){
        try {
            this.url = new URL(u);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run(){
    try {

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        FeedItem curFeedItem = null;
        Feed curFeed = null;
        String curText = null;

        xpp.setInput(this.url.openConnection().getInputStream(), null); // give the link to the parser

        int eventType = xpp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) { //loop through the pull events until END_DOCUMENT
            String tagname = xpp.getName(); // get current tag

            switch (eventType){
                case XmlPullParser.START_TAG:
                    if(tagname.equalsIgnoreCase("item")){
                        curFeedItem = new FeedItem(); // Create a new FeedItem object on <item>
                        Log.e("objekti tehään", "jee");

                    } else if(tagname.equalsIgnoreCase("channel")){ //Creates a new Feed object at <channel>
                        curFeed = new Feed();
                        User.getInstance() .addToFeeds(curFeed);
                    }
                    break;

                case XmlPullParser.TEXT:
                    curText = xpp.getText(); // get the text inside the item
                    break;

                case XmlPullParser.END_TAG:
                    if (tagname.equalsIgnoreCase("item")){ // if </item>, add to the list.
                        curFeed.addToFeedItems(curFeedItem);
                        curFeedItem.setChannel(curFeed.getFeedName());
                        Log.e("objektin lisäys", "jee");

                    } else if (tagname.equalsIgnoreCase("title") && curFeedItem==null){
                        curFeed.setFeedName(curText);

                    }else if (tagname.equalsIgnoreCase("title") && curFeedItem!=null){
                        curFeedItem.setTitle(curText);
                        Log.e("title: ", curText);

                    } else if (tagname.equalsIgnoreCase("description") && curFeedItem != null){
                        curFeedItem.setDescription(curText);
                        Log.e("description: ", curText);

                    } else if (tagname.equalsIgnoreCase("pubDate") && curFeedItem !=null){
                        curFeedItem.setPubDate(curText);
                        Log.e("pubDate: ", curText);

                    } else if (tagname.equalsIgnoreCase("link") && curFeedItem !=null){
                        curFeedItem.setLink(curText);
                        Log.e("linkki: ", curText);

                    }
                    break;

                default:
                    break;
            }
            eventType = xpp.next();
        }
    } catch (Exception e) {
        Log.e("FeedReader run", "error");
    }
        User.getInstance().notifyAllObservers();
}

}
