package com.example.eemeli.gonarss;

public interface Observer {
    void update();
}
