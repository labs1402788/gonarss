package com.example.eemeli.gonarss;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Observer {

    private ListView mDrawerList;
    private RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button addButton = (Button) findViewById(R.id.addFeed);
        final Button removeButton = (Button) findViewById(R.id.removeFeed);
        User.getInstance().notifyAllObservers();
        final FeedReader fr = new FeedReader("http://www.hs.fi/uutiset/rss/");
        User.getInstance().registerObserver(this);
        Thread t1 = new Thread(fr);
        t1.start();

        //Hamburger business
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        mDrawerList.setAdapter(new DrawerListAdapter(this, User.getInstance().getFeeds()));

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        //AddButton onClickListener business
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Add a feed");
                alert.setMessage("input a feed's URL");
                // Set an EditText view to get user input
                final EditText input = new EditText(MainActivity.this);
                alert.setView(input);
                alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        final FeedReader fr = new FeedReader(input.getText().toString());
                        Thread t2 = new Thread(fr);
                        t2.start();
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });
                alert.show();
            }
        });

        //RemoveButton onClickListener business
        removeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Remove feeds");
                String[] cs = new String[User.getInstance().getFeedsSize()];

                for (int i = 0; i < User.getInstance().getFeedsSize(); i++){
                    cs[ i ] = User.getInstance().getFeeds().get(i).getFeedName();
                }

                boolean bl[] = new boolean[cs.length];
                final ArrayList<Integer> selected = new ArrayList<Integer>(); // Where we track the selected items
                alert.setMultiChoiceItems(cs, bl, new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {
                        selected.add(which);

                    }
                });

                alert.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (selected.size()<User.getInstance().getFeedsSize()){
                            for (int i = 0; i < selected.size(); i++) {
                                User.getInstance().removeFromFeeds(User.getInstance().getFeeds().get(selected.get(i)));
                            }
                            Toast toast = Toast.makeText(MainActivity.this, selected.size() + " feed(s) deleted", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        else{
                            Toast toast2 = Toast.makeText(MainActivity.this, "You must have atleast 1 feed", Toast.LENGTH_SHORT);
                            toast2.show();
                        }
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });
                alert.create().show();
            }
        });
    }

    private void selectItemFromDrawer(final int position) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                final ListView myLV = (ListView)findViewById(R.id.ikkuna);
                myLV.setAdapter(new FeedAdapter(MainActivity.this, R.layout.row_layout, User.getInstance().getFeeds().get(position).getFeedItems()));
                mDrawerList.setAdapter(new DrawerListAdapter(MainActivity.this, User.getInstance().getFeeds()));
            }
        });
        mDrawerList.setItemChecked(position, true);
        setTitle(User.getInstance().getFeeds().get(position).getFeedName());
        // Close the drawer
        mDrawerLayout.closeDrawer(mDrawerPane);
    }

    // Called when invalidateOptionsMenu() is invoked
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerPane);
        return super.onPrepareOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle
        // If it returns true, then it has handled
        // the nav drawer indicator touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle other action bar items...
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    //Hamburger business end

    public void update(){
        final ListView myLV = (ListView)findViewById(R.id.ikkuna);
        this.runOnUiThread(new Runnable() {
            public void run() {
                myLV.setAdapter(new FeedAdapter(MainActivity.this, R.layout.row_layout, User.getInstance().getFeeds().get(0).getFeedItems()));
                mDrawerList.setAdapter(new DrawerListAdapter(MainActivity.this, User.getInstance().getFeeds()));
            }
        });
        myLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(User.getInstance().getFeeds().get(0).getFeedItems().get(position).getLink()));
                startActivity(myIntent);
            }
        });
    }
    
    //Saving business START
    @Override
    protected void onPause(){
    super.onPause();

        FileOutputStream outputStream;
        try{
            outputStream = openFileOutput("RssFeeds", Context.MODE_PRIVATE);
            ObjectOutputStream ous = new ObjectOutputStream(outputStream);
            ous.writeObject(User.getInstance().getFeeds());

            outputStream.close();
            ous.close();

            Log.e("write", "file");
        }
        catch (FileNotFoundException e){
            Log.e("error", e.toString());
        }catch (IOException e){
            Log.e("error", e.toString());
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        FileInputStream in = null;
        final ListView myLV = (ListView)findViewById(R.id.ikkuna);
        try{
            in = openFileInput("RssFeeds");
            ObjectInputStream ois = new ObjectInputStream(in);
            ArrayList<Feed> temp = ((ArrayList<Feed>)ois.readObject());
            in.close();
            ois.close();
            User.getInstance().replaceFeeds(temp);
            myLV.setAdapter(new FeedAdapter(MainActivity.this, R.layout.row_layout, User.getInstance().getFeeds().get(0).getFeedItems()));
            User.getInstance().notifyAllObservers();
        }catch (FileNotFoundException e){
            Log.e("error", e.toString());
        }catch (StreamCorruptedException e){
            Log.e("error", e.toString());
        } catch (ClassNotFoundException e) {
            Log.e("error", e.toString());
        } catch (OptionalDataException e) {
            Log.e("error", e.toString());
        } catch (IOException e) {
            Log.e("error", e.toString());
        }
    }
    //Saving business END
}
