package com.example.eemeli.gonarss;

import java.io.Serializable;
import java.util.ArrayList;

public class Feed implements Serializable {
    private String feedName;
    private String feedLink;
    private ArrayList<FeedItem> feedItems = new ArrayList<FeedItem>();

    public Feed(){

    }
    public void setFeedName(String s){
        this.feedName = s;
    }

    public String getFeedName(){
        return this.feedName;
    }

    public void setFeedLink(String s){
        this.feedLink = s;
    }

    public String getFeedLink(){
        return this.feedLink;
    }

    public void addToFeedItems(FeedItem fi){
        feedItems.add(fi);
    }

    public void feedItemsSize(){
        String.valueOf(feedItems.size());
    }

    public ArrayList<FeedItem> getFeedItems(){
        return feedItems;
    }
}