package com.example.eemeli.gonarss;

import java.io.Serializable;

public class FeedItem implements Serializable{

    private String title;
    private String pubDate;
    private String description;
    private String link;
    private String channel;

    public FeedItem(){
        this.title = null;
        this.pubDate = null;
        this.description = null;
        this.link = null;
        this.channel = null;
    }

    public void setTitle(String s){
        this.title = s;
    }
    public String getTitle(){
        return this.title;
    }

    public void setPubDate(String d){
        this.pubDate = d;
    }
    public String getPubDate(){
        return this.pubDate;
    }

    public void setDescription(String desc){
        this.description = desc;
    }
    public String getDescription(){
        return this.description;
    }

    public void setLink(String l){
        this.link = l;
    }
    public String getLink(){
        return this.link;
    }

    public void setChannel(String c){
        this.channel = c;
    }
    public String getChannel(){
        return this.channel;
    }
}
