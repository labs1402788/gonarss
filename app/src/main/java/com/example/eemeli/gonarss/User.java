package com.example.eemeli.gonarss;

import java.util.ArrayList;



//holds a list of all the feeds & observers

public class User{
    // Singleton business
    private static final User INSTANCE = new User();
    private User(){}
    public static User getInstance(){
        return INSTANCE;
    }

    //Observer business
    private ArrayList<Observer> observers = new ArrayList<Observer>();
    public void registerObserver(Observer o){
        observers.add(o);
    }
    public void unregisterObserver(Observer o){
        observers.remove(o);
    }
    public void notifyAllObservers(){
        for (Observer observer : observers) {
            observer.update();
        }
    }

    //Feed management
    private ArrayList<Feed> feeds = new ArrayList<Feed>();
    private ArrayList<FeedItem> allItems = new ArrayList<FeedItem>();

    public void addToFeeds(Feed f){
        feeds.add(f);
        notifyAllObservers();
    }

    public void removeFromFeeds(Feed f){
        feeds.remove(f);
        notifyAllObservers();
    }

    public ArrayList<Feed> getFeeds(){
        return feeds;
    }

    public int getFeedsSize(){
        return feeds.size();
    }

    public void replaceFeeds(ArrayList<Feed> replace){
        this.feeds = replace;
        notifyAllObservers();
    }

    public ArrayList<FeedItem> getAllItems(){
        for (int i = 0; i<feeds.size(); i++){
            for (int j = 0; j<getFeeds().get(i).getFeedItems().size(); j++)
                allItems.add(getFeeds().get(i).getFeedItems().get(j));
        }
        return allItems;
    }

}
